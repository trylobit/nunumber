#include<stdio.h>
#include<stdlib.h>

/* nunumber will be represented as a binary tree,
where right child points to nunumber, which should be added
and left child points to the power of current node */
typedef struct node {
  short int height;
  struct node *add; /*nunumber added */
  struct node *power; /*nunumber power */
} Node;

/*function that creates "empty" node */
Node * initTree() {
  Node *root = (Node *) malloc(sizeof(Node));
  root->height = 0;
  root->add = NULL;
  root->power = NULL;
  return root;
}

/*function that reads nunumber from stdin */
Node * getTree() {
  Node *root;
  Node *temp1;
  int c;

  root = initTree();
  temp1 = root;
  c = getchar();

  if(c == 'Z')
    return root;

  while(c == 'Y') {
    temp1->add = initTree();
    temp1 = temp1->add;
    temp1->power = getTree();
    c = getchar();
  }

  temp1 = root->add;
  free(root);
  return temp1;
}

/*function that frees the memory */
void freeTree(Node *root) {
  if(root->add != NULL)
    freeTree(root->add);
  if(root->power != NULL) \
    freeTree(root->power);
  free(root);
}

/*returns copy of a tree */
Node * copyTree(Node *root) {
  Node *res;
  if(root == NULL) return NULL;
  res = initTree();
  if(root->add != NULL) res->add = copyTree(root->add);
  if(root->power != NULL) res->power = copyTree(root->power);
  return res;
}

/*deletes root->add */
void deleteTreeAdd(Node *root) {
  Node *temp;
  if(root->add != NULL) {
    temp = root->add;
    root->add = temp->add;
    temp->add = NULL;
    freeTree(temp);
  }
}

/*function that evaluates tree height */
void heightTreePower(Node *root) {
  Node *temp;
  int max_height;
  if(root->power != NULL) {
    temp = root->power;
    heightTreePower(temp);
    max_height = temp->height;
    while(temp->add != NULL) {
      temp = temp->add;
      heightTreePower(temp);
      if(temp->height > max_height)
        max_height = temp->height;
    }
    root->height = max_height + 1;
  }
}

/*inserts "root" after "after" */
void insertAfterTree(Node *after, Node *root) {
 root->add = after->add;
 after->add = root;
}

/*functions that "reverses" tree with respect to addition */
Node * makeReverseTreePower(Node *root) {
  Node *reverse;
  Node *temp;
  if(root->power == NULL)
    return initTree();
  else {
    reverse = initTree();
    temp = root->power;
    reverse->power = initTree();
    while(temp != NULL) {
      insertAfterTree(reverse->power, makeReverseTreePower(temp));
      temp = temp->add;
    }
    temp = reverse->power;
    reverse->power = temp->add;
    free(temp);
    return reverse;
  }
}

/*deleting zeros from the end */
void cleanseNormTreePower(Node *root) {
  Node *temp;
  if(root->power != NULL) {
    temp = initTree();
    temp->add = root->power;
    root->power = temp;
    while(temp->add->add != NULL) {
      temp = temp->add;
      cleanseNormTreePower(temp);
    }
    if(temp->add->power == NULL) {
      deleteTreeAdd(temp);
    }
    else
      cleanseNormTreePower(temp->add);
    temp = root->power;
    if(temp->add != NULL) {
      root->power = temp->add;
      free(temp);
    }
  }
}

/*function that compares normalized nunumbers
0 -> they'ra equal
1 -> root1 > root2
2 -> root1 < root2
*/
int compareReverseNormTreePower(Node *root1, Node *root2) {
  Node *temp1;
  Node *temp2;
  int n;
  if(root1->height > root2->height)
    return 1;
  else if(root1->height < root2->height)
    return 2;
  else {
    if(root1->power == NULL && root2->power == NULL)
      return 0;
    else {
      temp1 = root1->power;
      temp2 = root2->power;
      do {
      n = compareReverseNormTreePower(temp1, temp2);
      if(n != 0) {
        return n;
      }
      temp1 = temp1->add;
      temp2 = temp2->add;
      }while(temp1 != NULL && temp2 != NULL);
      if(temp1 != NULL) {
        return 1;
      }
      else if(temp2 != NULL) {
        return 2;
      }
      else {
        return 0;
      }
    }
  }
}

/*function that adds powers of root1 and root2 and returns the result */
Node * sumTreePower(Node *root1, Node *root2) {
  Node *root;
  Node *temp;
  root = initTree();
  if(root1->power != NULL) {
    root->power = copyTree(root1->power);
    if(root2->power != NULL) {
      temp = root->power;
      while(temp->add != NULL) {
        temp = temp->add;
      }
      temp->add = copyTree(root2->power);
    }
  }
  else if(root2->power != NULL) {
    root->power = copyTree(root2->power);
  }
  cleanseNormTreePower(root);
  return root;
}

/*increments tree power by one */
void increaseTreePower(Node *root) {
  Node *temp;
  if(root->power != NULL) {
    if(root->power->power != NULL) {
    temp = initTree();
    temp->power = initTree();
    temp->add = root->power;
    root->power = temp;
    }
    else {
      root->power->power = initTree();
    }
  }
  else {
    root->power = initTree();
    root->power->power = initTree();
  }
}

/*looks for duplicates */
int hasDuplicatesReverseNormTreePower(Node *root) {
  Node *temp;
  if(root->power != NULL) {
    temp = root->power;
    while(temp->add != NULL) {
      if(compareReverseNormTreePower(temp, temp->add) == 0)
        return 1;
      temp = temp->add;
    }
  }
  return 0;
}

/*function that performs multiplication root1->power * root2->power */
Node * multiplyTreePower(Node *root1, Node *root2) {
  Node *result;
  Node *temp1;
  Node *temp2;
  Node *sum;
  result = initTree();
  result->power = initTree();
  temp1 = root1->power;
  while(temp1 != NULL) {
    temp2 = root2->power;
    while(temp2 != NULL) {
      sum = sumTreePower(temp1, temp2);
      if(sum != NULL)
        insertAfterTree(result->power, sum);
      temp2 = temp2->add;
    }
    temp1 = temp1->add;
  }
  temp1 = result->power;
  result->power = temp1->add;
  free(temp1);
  return result;
}

/*tree normalization */
void normalizeReverseTreePower(Node *root) {
  Node *temp;
  Node *start;
  Node *max_next;
  int n;
  if(root->power != NULL) {
    temp = root->power;
    while(temp != NULL) {
      normalizeReverseTreePower(temp);
      heightTreePower(temp);
      temp = temp->add;
    }
    start = initTree();
    start->add = root->power;
    root->power = start;
    while(start->add != NULL) {
      temp = start->add;
      max_next = start;
      while(temp->add != NULL) {
        n = compareReverseNormTreePower(max_next->add, temp->add);
        if(n == 0) {
          deleteTreeAdd(temp);
          increaseTreePower(max_next->add);
          heightTreePower(root);
          normalizeReverseTreePower(max_next->add);
        }
        else {
          if(n == 2)
            max_next = temp;
          temp = temp->add;
        }
      }
      temp = max_next->add;
      max_next->add = temp->add;
      insertAfterTree(start, temp);
      start = temp;
    }
    start = root->power;
    root->power = start->add;
    free(start);
    if(hasDuplicatesReverseNormTreePower(root))
      normalizeReverseTreePower(root);
  }
}

/*function that writes nunumber to stdout */
void printTree(Node * root) {
  if(root->power != NULL)
  {
    printf("Y");
    printTree(root->power);
    printf("Z");
  }
  if(root->add != NULL) {
    printTree(root->add);
  }
}

int main() {
  Node *ubernunumber1;
  Node *ubernunumber2;
  Node *result;
  Node *reverse;

  ubernunumber1 = initTree();
  ubernunumber1->power = getTree();
  getchar();
  ubernunumber2 = initTree();
  ubernunumber2->power = getTree();
  getchar();

  normalizeReverseTreePower(ubernunumber1);
  normalizeReverseTreePower(ubernunumber2);

  result = multiplyTreePower(ubernunumber1, ubernunumber2);
  freeTree(ubernunumber1);
  freeTree(ubernunumber2);

  normalizeReverseTreePower(result);
  cleanseNormTreePower(result);
  normalizeReverseTreePower(result);

  reverse = makeReverseTreePower(result);
  printTree(reverse->power);
  printf("Z");
  printf("\n");

  freeTree(result);
  freeTree(reverse);
  return 0;
}
